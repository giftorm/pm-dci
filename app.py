from numpy.random import seed
seed(101)
from tensorflow import set_random_seed
set_random_seed(101)

import pandas as pd
import numpy as np

import tensorflow

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Conv2D, MaxPooling2D, Flatten
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.metrics import categorical_crossentropy
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau, ModelCheckpoint

import os
import cv2
import sys

import imageio
import skimage
import skimage.io
import skimage.transform

from sklearn.utils import shuffle
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
import itertools
import shutil
import matplotlib.pyplot as plt
# %matplotlib inline

# Number of samples we want in each class.Total images used = SAMPLE_SIZE X 2
# The minority class is class 1 with 78786 samples.
SAMPLE_SIZE = 78786
IMAGE_SIZE = 50
EPOCHS_COUNT = 5

# Variables
PIC_PATH = os.path.abspath('C:/temp/dci/input/')
RUN_PATH = os.path.realpath('./')
RESOURCE_PATH = os.path.join(RUN_PATH, 'resources/')
ALL_IMAGES_DIR = os.path.join(RUN_PATH, 'resources/all_images/')
TRAIN_DATA_PATH = os.path.join(RUN_PATH, 'resources/train_data/')
TRAIN_DIR = os.path.join(TRAIN_DATA_PATH, 'train_dir/')
VAL_DIR = os.path.join(TRAIN_DATA_PATH, 'val_dir/')

def verify_picture_folder():
    # Check the number of patient folders.
    if len(os.listdir(PIC_PATH)) == 279:
        print("Amount of patients OK(279)")
    else:
        print("Something\'s wrong with the picture path\nVerify target folder, see README.md for more info.")

def gather_all_pictures(skip=0):
    # Create a new directory to store all available images
    resource_list = [RESOURCE_PATH, ALL_IMAGES_DIR]
    for i in resource_list: 
        try:
            os.mkdir(i)
            print("Folder {} successfully created.\n".format(i))
        except:
            if os.path.isdir(i):
                print("Folder already exists.")
            else:
                print("Error, cannot create folder. Try run as administrator.")
                sys.exit(5)
    
    # This code copies all images from their seperate folders into the same 
    # folder called ALL_IMAGES_DIR.

    # Create a list with all the patient id numbers.
    # Each patient id folder has 2 sub folders --> folder 0 and folder 1

    # Example:
        # '10285'
            # '0'
            # '1'

    # check how many images are in ALL_IMAGES_DIR
    # should be 277,524
    # size: 2.5GB   
    if len(os.listdir(ALL_IMAGES_DIR)) == 277524:
        print("All {} images exist.".format(len(os.listdir(ALL_IMAGES_DIR))))
    elif skip:
        print("Skipping creation of images cus of attribute")
    else:
        print("Copying all images to one folder: {}.".format(ALL_IMAGES_DIR))
        # create a list of all patient id's
        
        patient_list = os.listdir(PIC_PATH)

        for patient in patient_list:  
            path_0 = os.path.join(PIC_PATH, str(patient) + '/0')
            path_1 = os.path.join(PIC_PATH, str(patient) + '/1')

            # create a list of all files in folder 0
            file_list_0 = os.listdir(path_0)
            # create a list of list all file in folder 1
            file_list_1 = os.listdir(path_1)

            # move the 0 images to ALL_IMAGES_DIR
            for fname in file_list_0:
                # source path to image
                src = os.path.join(path_0, fname)
                # destination path to image
                dst = os.path.join(ALL_IMAGES_DIR, fname)
                # copy the image from the source to the destination
                shutil.copyfile(src, dst)

            # move the 1 images to ALL_IMAGES_DIR
            for fname in file_list_1:
                # source path to image
                src = os.path.join(path_1, fname)
                # destination path to image
                dst = os.path.join(ALL_IMAGES_DIR, fname)
                # copy the image from the source to the destination
                shutil.copyfile(src, dst)
        print("All {} images created.".format(len(os.listdir(ALL_IMAGES_DIR))))

def create_data_frame():
    print("Create a dataframe containing all the information:")
    image_list = os.listdir(ALL_IMAGES_DIR)
    df_data = pd.DataFrame(image_list, columns=['image_id'])
    print("\n{}".format(df_data.head()))
    return df_data

# Define Helper Functions

# Each file name has this format:
# '14211_idx5_x2401_y1301_class1.png'

def extract_patient_id(x):
    # split into a list
    a = x.split('_')
    # the id is the first index in the list
    patient_id = a[0]
    
    return patient_id

def extract_target(x):
    # split into a list
    a = x.split('_')
    # the target is part of the string in index 4
    b = a[4]
    # the ytarget i.e. 1 or 2 is the 5th index of the string --> class1
    target = b[5]
    
    return target

def ectract_patient_id(df_data):    
    print("\nExtract the patient id.")
    print("Create a new column called \'patient_id\'.")
    df_data['patient_id'] = df_data['image_id'].apply(extract_patient_id)
    print("Create a new column called \'target\'")
    df_data['target'] = df_data['image_id'].apply(extract_target)
    print("\n{}".format(df_data.head(10)))
    print("df_data.shape: {}".format(df_data.shape))
    return df_data
    
def balance_class_distribution(df_data):
    print("\nBalance the class distribution:")
    # What is the class distribution?

    print("{}\n".format(df_data['target'].value_counts()))

    # take a sample of the majority class 0 (total = 198738)
    df_0 = df_data[df_data['target'] == '0'].sample(SAMPLE_SIZE, random_state=101)
    # # take a sample of class 1 (total = 78786)
    df_1 = df_data[df_data['target'] == '1'].sample(SAMPLE_SIZE, random_state=101)

    # # concat the two dataframes
    df_data = pd.concat([df_0, df_1], axis=0).reset_index(drop=True)

    # Check the new class distribution
    print("Checking the new class dist:")
    print(df_data['target'].value_counts())
    return df_data

def create_train_and_val_sets(df_data):
    print ("\nCreate the train and val sets")
    # train_test_split
    # stratify=y creates a balanced validation set.
    y = df_data['target']

    df_train, df_val = train_test_split(df_data, test_size=0.10, random_state=101, stratify=y)

    print(df_train.shape)
    print(df_val.shape)
    print()
    print(df_train['target'].value_counts())
    return df_train, df_val, df_data

def create_directory_structure():
    # Create a new directory
    create_folder(TRAIN_DATA_PATH, "Training and Validation")
    create_folder(TRAIN_DIR, "train")
    create_folder(VAL_DIR, "validation")

    # [CREATE FOLDERS INSIDE THE TRAIN AND VALIDATION FOLDERS]
    # Inside each folder we create seperate folders for each class

    # create new folders inside TRAIN_DIR
    a_no_idc = os.path.join(TRAIN_DIR, 'a_no_idc')
    create_folder(a_no_idc, "a_no_idc")
    b_has_idc = os.path.join(TRAIN_DIR, 'b_has_idc')
    create_folder(b_has_idc, "b_has_idc")


    # create new folders inside VAL_DIR
    a_no_idc = os.path.join(VAL_DIR, 'a_no_idc')
    create_folder(a_no_idc, "a_no_idc")
    b_has_idc = os.path.join(VAL_DIR, 'b_has_idc')
    create_folder(b_has_idc, "b_has_idc")

    # check that the folders have been created
    print("Folders successfully created: {}\n".format(os.listdir(TRAIN_DIR)))

def create_folder(path, name):
    try:
        os.mkdir(path)
        print("{} folder created.\nPath: {}\n".format(name.title(), path))
    except:
        if os.path.isdir(path):
            print("{} folder already exists.\n".format(name.title()))
        else:
            print("Error trying to create {} folder\n".format(name))

def transfer_images_to_folders(df_data, df_train, df_val):
    print("Transfer images into the trainig data folders\n")
    # Set the id as the index in df_data
    df_data.set_index('image_id', inplace=True)

    # Get a list of train and val images
    train_list = list(df_train['image_id'])
    val_list = list(df_val['image_id'])

    if len(os.listdir(os.path.join(VAL_DIR, 'a_no_idc'))) == 7879:
        print("All files successfully moved to train_dir/a_no_idc! ({})\n".format('7879'))
    else:
        # Transfer the train images
        for image in train_list:
            # the id in the csv file does not have the .tif extension therefore we add it here
            fname = image
            # get the label for a certain image
            target = df_data.loc[image,'target']
            # these must match the folder names
            if target == '0':
                label = 'a_no_idc'
            if target == '1':
                label = 'b_has_idc'
            
            # source path to image
            src = os.path.join(ALL_IMAGES_DIR, fname)
            # destination path to image
            dst = os.path.join(TRAIN_DIR, label, fname)
            # move the image from the source to the destination
            shutil.copyfile(src, dst)

    if len(os.listdir(os.path.join(VAL_DIR, 'b_has_idc'))) == 7879:
        print("All files successfully moved to train_dir/a_no_idc! ({})\n".format('7879'))
    else:
        # Transfer the val images
        for image in val_list:
            # the id in the csv file does not have the .tif extension therefore we add it here
            fname = image
            # get the label for a certain image
            target = df_data.loc[image,'target']
            
            # these must match the folder names
            if target == '0':
                label = 'a_no_idc'
            if target == '1':
                label = 'b_has_idc'

            # source path to image
            src = os.path.join(ALL_IMAGES_DIR, fname)
            # destination path to image
            dst = os.path.join(VAL_DIR, label, fname)
            # move the image from the source to the destination
            shutil.copyfile(src, dst)

# check how many train images we have in each folder
if __name__ == '__main__':
    verify_picture_folder()
    gather_all_pictures(skip=0)
    df_data = create_data_frame()
    df_data = ectract_patient_id(df_data)
    df_data = balance_class_distribution(df_data)
    df_train, df_val, df_data = create_train_and_val_sets(df_data)
    create_directory_structure()
    transfer_images_to_folders(df_data, df_train, df_val)

    # plizzz fix... >.<
    train_path = TRAIN_DIR
    valid_path = VAL_DIR

    num_train_samples = len(df_train)
    num_val_samples = len(df_val)
    train_batch_size = 10
    val_batch_size = 10


    train_steps = np.ceil(num_train_samples / train_batch_size)
    val_steps = np.ceil(num_val_samples / val_batch_size)
    datagen = ImageDataGenerator(rescale=1.0/255)

    train_gen = datagen.flow_from_directory(train_path,
                                            target_size=(IMAGE_SIZE,IMAGE_SIZE),
                                            batch_size=train_batch_size,
                                            class_mode='categorical')

    val_gen = datagen.flow_from_directory(valid_path,
                                            target_size=(IMAGE_SIZE,IMAGE_SIZE),
                                            batch_size=val_batch_size,
                                            class_mode='categorical')

    # Note: shuffle=False causes the test dataset to not be shuffled
    test_gen = datagen.flow_from_directory(valid_path,
                                            target_size=(IMAGE_SIZE,IMAGE_SIZE),
                                            batch_size=1,
                                            class_mode='categorical',
                                            shuffle=False)
    
    # Source: https://www.kaggle.com/fmarazzi/baseline-keras-cnn-roc-fast-5min-0-8253-lb

    kernel_size = (3,3)
    pool_size= (2,2)
    first_filters = 32
    second_filters = 64
    third_filters = 128

    dropout_conv = 0.3
    dropout_dense = 0.3


    model = Sequential()
    model.add(Conv2D(first_filters, kernel_size, activation = 'relu', 
                    input_shape = (IMAGE_SIZE, IMAGE_SIZE, 3)))
    model.add(Conv2D(first_filters, kernel_size, activation = 'relu'))
    model.add(Conv2D(first_filters, kernel_size, activation = 'relu'))
    model.add(MaxPooling2D(pool_size = pool_size)) 
    model.add(Dropout(dropout_conv))

    model.add(Conv2D(second_filters, kernel_size, activation ='relu'))
    model.add(Conv2D(second_filters, kernel_size, activation ='relu'))
    model.add(Conv2D(second_filters, kernel_size, activation ='relu'))
    model.add(MaxPooling2D(pool_size = pool_size))
    model.add(Dropout(dropout_conv))

    model.add(Conv2D(third_filters, kernel_size, activation ='relu'))
    model.add(Conv2D(third_filters, kernel_size, activation ='relu'))
    model.add(Conv2D(third_filters, kernel_size, activation ='relu'))
    model.add(MaxPooling2D(pool_size = pool_size))
    model.add(Dropout(dropout_conv))

    model.add(Flatten())
    model.add(Dense(256, activation = "relu"))
    model.add(Dropout(dropout_dense))
    model.add(Dense(2, activation = "softmax"))

    model.summary()

    model.compile(Adam(lr=0.0001), loss='binary_crossentropy', 
              metrics=['accuracy'])

    filepath = "model.h5"
    checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, 
                                save_best_only=True, mode='max')

    reduce_lr = ReduceLROnPlateau(monitor='val_acc', factor=0.5, patience=3, 
                                    verbose=1, mode='max', min_lr=0.00001)
                                
                                
    callbacks_list = [checkpoint, reduce_lr]

    history = model.fit_generator(train_gen, steps_per_epoch=train_steps, 
                        validation_data=val_gen,
                        validation_steps=val_steps,
                        epochs=EPOCHS_COUNT, verbose=1,
                    callbacks=callbacks_list)

    # get the metric names so we can use evaulate_generator
    model.metrics_names

    # Here the best epoch will be used.

    model.load_weights('model.h5')

    val_loss, val_acc = \
    model.evaluate_generator(test_gen, 
                            steps=len(df_val))

    print('val_loss:', val_loss)
    print('val_acc:', val_acc)

    # display the loss and accuracy curves

    import matplotlib.pyplot as plt

    acc = history.history['acc']
    val_acc = history.history['val_acc']
    loss = history.history['loss']
    val_loss = history.history['val_loss']

    epochs = range(1, len(acc) + 1)

    plt.plot(epochs, loss, 'bo', label='Training loss')
    plt.plot(epochs, val_loss, 'b', label='Validation loss')
    plt.title('Training and validation loss')
    plt.legend()
    plt.figure()

    plt.plot(epochs, acc, 'bo', label='Training acc')
    plt.plot(epochs, val_acc, 'b', label='Validation acc')
    plt.title('Training and validation accuracy')
    plt.legend()
    plt.figure()
    # DISPLAY IN CMD
    plt.show()

    # make a prediction
    predictions = model.predict_generator(test_gen, steps=len(df_val), verbose=1)
    print(predictions.shape)

    # This is how to check what index keras has internally assigned to each class. 
    print(test_gen.class_indices)

    # Put the predictions into a dataframe.
    # The columns need to be oredered to match the output of the previous cell

    df_preds = pd.DataFrame(predictions, columns=['no_idc', 'has_idc'])

    df_preds.head()

    # Get the true labels
    y_true = test_gen.classes

    # Get the predicted labels as probabilities
    y_pred = df_preds['has_idc']
    
    from sklearn.metrics import roc_auc_score

    print("Roc_auc_sore: {}".format(roc_auc_score(y_true, y_pred)))






